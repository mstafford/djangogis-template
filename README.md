# DjangoGIS-Template

Blank slate for Django GIS enable webapps. Ability to add STEEM support after. General ideas: EXHAUST, SANCUS, BUDS, STORE, ASSETS UNCHAINED

# For Ubuntu w/ NGinx (DigitalOcean server)
## Install Dependencies
```
sudo apt-get install python3-pip python3-dev libpq-dev postgresql postgresql-contrib postgis nginx libssl-dev
```

## Install Geospatial Libraries:
```
sudo apt-get install binutils libproj-dev gdal-bin
```
## Install Python Dependencies:
```
pip install -r requirements.txt
```
