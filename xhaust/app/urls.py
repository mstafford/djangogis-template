from django.conf.urls import url, include
from django.urls import path

from rest_framework.urlpatterns import format_suffix_patterns

from . import views

urlpatterns = [
    path('', views.dash, name='homelanding'),
    path('api/v1/', views.api_root, name='apihome'),
    path('dash/', views.dash, name='dash'),
    path('dash/wallet/', views.wallet, name="wallet"),
    path('dash/<str:sport>/', views.dash, name="dash"),
    path('c/<str:sort>/', views.community, name='community'),
    path('@<str:author>/<str:permlink>/', views.hivepost, name="viewpost"),
    path('<str:sort>/<str:tag>/', views.explore, name='explore'),
    path('<str:maintag>/@<str:author>/<str:permlink>/', views.hivepost, name="viewpost"),
    path('@<str:username>/', views.profile, name='profile'),
]

urlpatterns = format_suffix_patterns(urlpatterns,allowed=['json','html'])
