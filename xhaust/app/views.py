from app import utils
from app.community import ExhaustCommunity
from app.models import Activity, UserProfile

from asgiref.sync import sync_to_async

from beem.account import Account
from beem.comment import Comment
from beem.discussions import Discussions, Query, Post_discussions_by_payout, Discussions_by_created
from bs4 import BeautifulSoup

from datetime import datetime

from django.contrib.auth.models import User
from django.contrib.gis.geoip2 import GeoIP2
from django.contrib.gis import geos

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render
from django.template import loader

from markdown2 import Markdown

from pycoingecko import CoinGeckoAPI

from rest_framework.decorators import api_view
from rest_framework.response import Response

import re

# Create your views here.

def community(request, sort):
    context = {}
    if request.user.is_authenticated:
        account, context = utils.my_account(request.user.username,context)
    community = ExhaustCommunity()
    if sort == "trending":
        context['community'] = community.exhaust_trending()
        context['sort'] = "Trending"
    elif sort == "created":
        context['community'] = community.exhaust_new()
        context['sort'] = "New"
    elif sort == "hot":
        context['community'] = community.exhaust_hot()
        context['sort'] = "Hot"
    else:
        context['community'] = community.exhaust_new()
        context['sort'] = "New"
    date_str = "%Y-%m-%dT%H:%M:%S"
    for post in context['community']:
         post['created'] = datetime.strptime(post['created'],date_str)
         user, created = User.objects.get_or_create(username=post['author'])
         profile, created = UserProfile.objects.get_or_create(user=user)
         if created:
             account = Account(user.username)
             profile.avatar = account.json_metadata['profile']['profile_image']
             profile.save()
         post['avatar'] = profile.avatar
    template = loader.get_template('app/tables.html')
    ip = request.META.get('HTTP_X_REAL_IP')
    g = GeoIP2()
    context['lat'],context['lon'] = g.lat_lon(ip)
    return HttpResponse(template.render(context, request))

def explore(request, sort, tag):
    context = {}
    if request.user.is_authenticated:
        account, context = utils.my_account(request.user.username, context)
    template = loader.get_template('app/world.html')
    q = Query(limit=30, tag=tag)
    context['posts'] = []
    if sort == "created":
        for post in Discussions_by_created(q):
            context['posts'].append(post)
    elif sort == "trending":
        for post in Post_discussions_by_payout(q):
            context['posts'].append(post)
    return HttpResponse(template.render(context, request))

def hivepost(request, author, permlink, maintag=None):
    context = {}
    if request.user.is_authenticated:
        account, context = utils.my_account(request.user.username,context)
    if "xhst-activity" in permlink:
        template = loader.get_template('app/activitypost.html')
        act = Activity.objects.get(hive_permlink=f"@{author}/{permlink}")
        context = utils.activity_performance(act, context)
    else:
        template = loader.get_template('app/hivepost.html')
    context = utils.post_performance(f"@{author}/{permlink}",context)
    context['comment'] = Comment(f"@{author}/{permlink}")
    markdowner = Markdown(extras=["tables"])
    body = markdowner.convert(context['comment'].body)
    soup = BeautifulSoup(body)
    images = soup.find_all('img')
    context['replies'] = context['comment'].get_replies()
    for img in images:
        img['style']='max-width:80%;display:block;margin-left:auto;margin-right:auto'
        if "gif" not in img['src']:
#            img['src']= 'https://images.hive.blog/640x480/' + img['src'] ## TO BE FIXED?
            img['src'] = 'https://steemitimages.com/640x480/' + img['src']
            img['height']=''
    tables = soup.find_all('table')
    for table in tables:
        table['align']="center"
        table['style']="min-width:80%"
    ip = request.META.get('HTTP_X_REAL_IP')
    g = GeoIP2()
    context['lat'],context['lon'] = g.lat_lon(ip)
    context['soup']=soup.prettify()
    return HttpResponse(template.render(context, request))

@login_required(login_url="/a/")
def dash(request, sport="all"):
    context = {}
    if request.user.is_authenticated:
        account, context = utils.my_account(request.user.username,context)
        context = utils.my_activities(request.user.username, context)
    context['acts'] = Activity.objects.filter(user=request.user)
    template = loader.get_template('app/dash_general.html')
    ip = request.META.get('HTTP_X_REAL_IP')
    g = GeoIP2()
    context['lat'],context['lon'] = g.lat_lon(ip)
    return HttpResponse(template.render(context, request))

#@sync_to_async
def find_outgoing_delegations(account):
    delegations_out = {}
    for h in account.history(only_ops=['delegate_vesting_shares']): ##causes site timeout. Can this be Async?
        delegations_out[h['delegatee']] = h['vesting_shares']['amount']
    return delegations_out


def wallet_info(account, context):
    current_hp = account.get_balances()['available'][2]
    stop = datetime.fromtimestamp(datetime.now().timestamp() - 60*60*24*7)
    power_ups = []
    cg = CoinGeckoAPI()
    context['prices'] = cg.get_price(ids='hive, hive_dollar', vs_currencies='usd,cad,btc')
    context['coins'] = {
        "hive":cg.get_coin_by_id('hive'),
        "hive_dollar":cg.get_coin_by_id('hive_dollar'),
    }
    context['balances'] = account.get_balances()
    context['balances']['available'][2] = f"{round(account.steem.vests_to_sp(context['balances']['available'][2]),3)} HIVE POWER"
#    context['balances']['rewards'][2] = f"{round(account.steem.vests_to_sp(context['balances']['rewards'][2]),3)} HIVE POWER"
    context['recent_transfers'] = []
    for h in account.history_reverse(only_ops=["claim_reward_balance", "transfer", "transfer_to_vesting"], stop=stop):
        h['timestamp'] = datetime.strptime(h['timestamp'],'%Y-%m-%dT%H:%M:%S')
        context['recent_transfers'].append(h)
#        context['delegations_out'] = find_outgoing_delegations(account)
    return context

@login_required(login_url="/a/")
def wallet(request):
    context = {}
    if request.user.is_authenticated:
        account, context = utils.my_account(request.user.username, context)
    template = loader.get_template('app/wallet.html')
    context = wallet_info(account,context)
    return HttpResponse(template.render(context, request))

def login(request):
    context = {}
    template = loader.get_template('app/login.html')
    ip = request.META.get('HTTP_X_REAL_IP')
    g = GeoIP2()
    context['lat'],context['lon'] = g.lat_lon(ip)
    return HttpResponse(template.render(context, request))

def profile(request, username):
    context = {}
    if request.user.is_authenticated:
        account, context = utils.my_account(request.user.username,context)
    template = loader.get_template('app/profile.html')
    ip = request.META.get('HTTP_X_REAL_IP')
    g = GeoIP2()
    context['lat'],context['lon'] = g.lat_lon(ip)
    account = Account(username)
    context['profile'] = account
    try:
        context['profile_avatar'] = account.profile['profile_image']
    except:
        context['profile_avatar'] = 'null'
    try:
        context['profile_friends'] = account.get_follow_count()
    except:
        context['profile_friends'] = 'null'

    return HttpResponse(template.render(context, request))


@api_view(['GET'])
def api_root(request, format=None):
    return Response({
#        'users': reverse('user-list', request=request, format=format),
    })


