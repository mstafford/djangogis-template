from django.contrib.auth.models import User
from django.contrib.gis.db import models

from datetime import datetime

# Create your models here.

class UserProfile(models.Model):
    SIGNER_CHOICES = [
        ("HIVESIGNER","HiveSigner"),
        ("HIVEKEYCHAIN","HiveKeychain"),
    ]
    avatar = models.CharField(max_length=256, default="")
    preferred_signer = models.CharField(max_length=32, default="HiveSigner", choices=SIGNER_CHOICES)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    email = models.CharField(max_length=128, default="")
    name_first = models.CharField(max_length=64, default="")
    name_last = models.CharField(max_length=64, default="")
    address_street = models.CharField(max_length=128, default="")
    address_city = models.CharField(max_length=64, default="")
    address_country = models.CharField(max_length=32, default="")
    address_postal = models.CharField(max_length=16, default="")
    def __str__(self):
        return f"{self.user.username} - Profile"

class Echo(models.Model):
    subject = models.CharField(max_length=100)
    body = models.TextField(max_length=2048)
    def __str__(self):
        return self.subject

class ActivityType(models.Model):
    type = models.CharField(max_length=64)
    def __str__(self):
        return self.type

class Activity(models.Model):
    activitytype = models.ForeignKey(ActivityType, default=0, on_delete=models.CASCADE)
    comments = models.CharField(max_length=1024*10, blank=True)
    date = models.DateTimeField(auto_now_add = False)
    posted_date = models.DateTimeField(auto_now_add = True)
    hive_posted = models.BooleanField(default=False)
    hive_permlink = models.CharField(default = '', max_length=128)
    hive_rewards = models.FloatField(default = 0.0)
    hive_tags = models.CharField(default='',max_length=1024)
    hive_title = models.CharField(default='',max_length=128)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    data_file = models.FileField(upload_to='uploads/activity_data/', blank=True)
    data_processed = models.TextField(max_length=1024*16, blank=True)
    json_data = models.TextField(max_length=1024*10, blank=True)
    geom_point = models.PointField(blank=True)
    geom_link = models.LineStringField(blank=True)

    photo = models.ImageField(upload_to='uploads/images/', blank=True)
    photo2 = models.ImageField(upload_to='uploads/images/', blank=True)
    photo3 = models.ImageField(upload_to='uploads/images/', blank=True)

