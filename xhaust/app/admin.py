from django.contrib.gis import admin
from .models import Activity, ActivityType


# Register your models here.
admin.site.register(Activity, admin.GeoModelAdmin)
admin.site.register(ActivityType, admin.GeoModelAdmin)
