from beem.comment import Comment
from beem.account import Account

import json
import requests

class ExhaustCommunity:
    base_url = 'https://api.hive.blog'
    data = '{"jsonrpc":"2.0", "method":"bridge.get_ranked_posts", "params":{"sort":"trending","tag":"hive-176853","observer":"exhaust"}, "id":1}'

    def exhaust_details(self):
        self.data = '{"jsonrpc":"2.0", "method":"bridge.get_community", "params":{"name":"hive-176853","observer":"exhaust"}, "id":1}'
        response = requests.post(self.base_url, data=self.data)
        if response.status_code == 200:
            return(response.json()['result'])
        elif response.status_code == 400:
            return(response)

    def exhaust_trending(self):
        data = json.loads(self.data)
        data['params']['sort'] = 'trending'
        self.data = json.dumps(data)
        response = requests.post(self.base_url, data=self.data)
        if response.status_code == 200:
            return(response.json()['result'])
        elif response.status_code == 400:
            return(response)

    def exhaust_hot(self):
        data = json.loads(self.data)
        data['params']['sort'] = 'hot'
        self.data = json.dumps(data)
        response = requests.post(self.base_url, data=self.data)
        if response.status_code == 200:
            return(response.json()['result'])
        elif response.status_code == 400:
            return(response)

    def exhaust_new(self):
        data = json.loads(self.data)
        data['params']['sort'] = 'created'
        self.data = json.dumps(data)
        response = requests.post(self.base_url, data=self.data)
        if response.status_code == 200:
            return(response.json()['result'])
        elif response.status_code == 400:
            return(response)

    def exhaust_promoted(self):
        data = json.loads(self.data)
        data['params']['sort'] = 'promoted'
        self.data = json.dumps(data)
        response = requests.post(self.base_url, data=self.data)
        if response.status_code == 200:
            return(response.json()['result'])
        elif response.status_code == 400:
            return(response)

    def exhaust_payout(self):
        data = json.loads(self.data)
        data['params']['sort'] = 'payout'
        self.data = json.dumps(data)
        response = requests.post(self.base_url, data=self.data)
        if response.status_code == 200:
            return(response.json()['result'])
        elif response.status_code == 400:
            return(response)


    def exhaust_payout_comments(self):
        data = json.loads(self.data)
        data['params']['sort'] = 'payout_comments'
        self.data = json.dumps(data)
        response = requests.post(self.base_url, data=self.data)
        if response.status_code == 200:
            return(response.json()['result'])
        elif response.status_code == 400:
            return(response)

