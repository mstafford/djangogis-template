from beem.account import Account
from beem.steem import Steem

from django import template

register = template.Library()
s= Steem()

@register.filter(name="format_nai")
def format_tokens(amount_dict):
    if amount_dict['nai'] == "@@000000021": #HIVE
        return f"{int(amount_dict['amount'])/1000} HIVE"
    elif amount_dict['nai'] == "@@000000013": #HBD
        return f"{int(amount_dict['amount'])/1000} HBD"
    elif amount_dict['nai'] == "@@000000037": #HIVEPOWER
        return f"{round(s.vests_to_sp(int(amount_dict['amount'])/1000000),3)} HIVEPOWER"
