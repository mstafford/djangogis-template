from app.models import Activity, ActivityType
from django.contrib.auth.models import User

from beem.account import Account
from beem.comment import Comment
from datetime import datetime

import json
import pytz

def activity_performance(act, context):
    context['act_stats'] = []
    if act.json_data != "":
        jdata = eval(act.json_data)
        for dataset in jdata:
            newstat  = {}
            newstat['name'] = dataset['type']
            if dataset['type'] == 'altitude':
                newstat['value'] = dataset['data'][-1]
                newstat['metric'] = "m"
            elif dataset['type'] == 'distance':
                newstat['value'] = round(dataset['data'][-1]/1000,2)
                newstat['metric'] = "km"
            elif dataset['type'] == 'time':
                newstat['value'] = round(dataset['data'][-1] / 60,2)
                newstat['metric'] = "min"
            elif dataset['type'] == "heartrate":
                newstat['value'] = dataset['data'][-1]
                newstat['metric'] = "BPM"
            newstat['updown'] = "down"
            newstat['comparison'] = 11.2
            newstat['timeframe'] = "In the last 20 days"
            print(newstat)
            context['act_stats'].append(newstat)


    return context

def post_performance(permlink, context):
    comment = Comment(permlink)
    context['upvotes'] = 0
    context['downvotes'] = 0
    votes = comment.get_votes()
    for vote in votes:
        if vote['weight'] >= 0:
            context['upvotes'] += 1
        else:
            context['downvotes'] += 1
    return context

def my_account(username, context):
    account = Account(username)
    context['my_account'] = account
    try:
        context['my_profile'] = account.json_metadata
    except:
        context['my_profile'] = 'null'
    try:
        context['my_avatar'] = account.profile['profile_image']
    except:
        context['my_avatar'] = 'null'
    try:
        context['my_notifications'] = account.get_notifications()
    except:
        context['my_notifications'] = 'null'
    try:
        context['my_friends'] = account.get_follow_count()
    except:
        context['my_friends'] = 'null'
    return account, context

def update_sport_types():
    acts = Activity.objects.all()
    atypes = ActivityType.objects.all().order_by('pk')
    for act in acts:
        if act.activitytype.pk == 4:
            print("Found climb labelled as swim! Correcting!")
            print(act.activitytype.pk, act.activitytype.type)
            act.activitytype = atypes[6]
            act.save()
        elif act.activitytype.pk == 7:
            print("Found swim labelled as climb! Correcting!")
            act.activitytype = atypes[3]
            act.save()

def my_activities(username, context):
    user = User.objects.get(username=username)
    acts = Activity.objects.filter(user=user)
    sports = ActivityType.objects.all().order_by('pk')
    utc = pytz.UTC
    now = utc.localize(datetime.now())
    lastthirty = utc.localize(datetime.fromtimestamp(now.timestamp()-60*60*24*30))
    lastseven = utc.localize(datetime.fromtimestamp(now.timestamp()-60*60*24*7))
    activities = {'running':{},'cycling':{},'hiking':{},'climbing':{}, 'yoga':{}, 'strength':{},'swimming':{}, 'recent':[], 'count_all':[], 'count_seven':[],'count_thirty':[]}
    for sport in sports:
        activities[sport.type] = {
            'total_dist':0,
            'total_time':0,
        }
        sport_acts = acts.filter(activitytype=sport)
        sport_week = sport_acts.filter(date__gte=lastseven)
        sport_month = sport_acts.filter(date__gte=lastthirty)
        activities['count_all'].append(len(sport_acts))
        activities['count_seven'].append(len(sport_week))
        activities['count_thirty'].append(len(sport_month))
    recent = acts.order_by('posted_date').reverse()[:10]
    for act in recent:
        activities['recent'].append([Comment(act.hive_permlink),act])
    context['activities'] = activities
    return context

def account_growth(account, context):
    stop = datetime.fromtimestamp(datetime.now().timestamp() - 60*60*24*365)
    hp_history = []   
    current_hp = account.steem.vests_to_sp(account.get_balances()['available'][2].amount)
    hp_history.append(round(current_hp,3))
    for h in account.history_reverse(only_ops=['transfer_to_vesting', 'fill_vesting_withdraw', 'claim_reward_balance'], stop=stop):
        if h['type'] == 'transfer_to_vesting':
            hp_history.append(round(hp_history[-1]-int(h['amount']['amount'])/1000,3))
        elif h['type'] == 'fill_vesting_withdraw':
            withdrawn = account.steem.vests_to_sp(int(h['withdrawn']['amount'])/1000000)
            hp_history.append(round(hp_history[-1]+withdrawn,3))
        elif h['type'] == 'claim_reward_balance':
            claimed = account.steem.vests_to_sp(int(h['reward_vests']['amount'])/1000000)
            hp_history.append(round(hp_history[-1]-claimed,3))
    context['hp_history'] = hp_history
    return(context)

