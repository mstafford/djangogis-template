// Send Handshake event
$("#sw-handshake").click(function() {
    hive_keychain.requestHandshake(function() {
        console.log('Handshake received!');
    });
});

// All transactions are sent via a swRequest event.

// Send decryption request
$("#send_decode").click(function() {
    hive_keychain.requestVerifyKey($("#decode_user").val(), $("#decode_message").val(), $("#decode_method option:selected").text(), function(response) {
        console.log('main js response - verify key');
        console.log(response);
    });
});

// Send decryption request
$("#xhst_user_search").click(function() {
    console.log('looking for user');
    window.location.href = "http://xhaust.me/@"+$("#xhst_user_name").val()+"/";
});

$("#claim_rewards").click(function() {

    var claim = {
        "account":$("#claim_account").val(),
        "reward_steem":$("#claim_HIVE").val()+" STEEM",
        "reward_sbd":$("#claim_HBD").val()+" SBD",
        "reward_vests":$("#claim_VESTS").val()+ " VESTS",
    };

    var claim_operations = [
        ["claim_reward_balance",claim],
    ];

    if (window.hive_keychain) {
        hive_keychain.requestBroadcast($("#claim_account").val(), claim_operations, "Posting", function(response) {
            console.log('main js response - broadcast');
            console.log('claiming rewards via keychain');
            console.log(response);
             if (response.success) {
    //        Queue Vote Here
//              queue_vote()
              window.location.href = "http://beta.xhaust.me/dash/wallet/";
            }
        });
    }

});


// Send post request
$("#post_blog").click(function() {
    var json_data = {
        app:"exhaust-beta/0.2",
        tags:$("#id_tags").tagsinput('items')
//        tags:["exhaust","test"],
    };
    var comment = {
        parent_author:"",
        parent_permlink:"exhaust",
        author:$("#comment_author").val(),
        permlink:$("#comment_perm").val(),
        title:$("#id_title").val(),
        body:$("#id_post_body").val(),
        json_metadata:JSON.stringify(json_data),
    };
    var commentops = {
        author:$("#comment_author").val(),
        permlink:$("#comment_perm").val(),
        max_accepted_payout:"100000.000 SBD",
        percent_steem_dollars:10000,
        allow_votes:true,
        allow_curation_rewards:true,
        extensions:[[
            0,
            {"beneficiaries":[{"account":"exhaust","weight":200}]}
        ]]
    };
    var post_operations = [
        ["comment",comment],
        ["comment_options",commentops],
    ];
    if (window.hive_keychain) {
        hive_keychain.requestBroadcast($("#comment_author").val(), post_operations, "Posting", function(response) {
            console.log('main js response - broadcast');
            console.log('creating activity via keychain');
            console.log(response);
             if (response.success) {
    //        Queue Vote Here
//              queue_vote()
              window.location.href = "http://xhaust.me/exhaust/@"+$("#comment_author").val()+"/"+$("#comment_perm").val();
            }
        });
    } //else {
//      sc_activity()
//    }
});

// Send post request
$("#post_activity").click(function() {
    var json_data = {
        app:"exhaust-beta/0.2",
        tags:$("#id_tags").tagsinput('items')
//        tags:["exhaust","test"],
    };
    var comment = {
        parent_author:"",
//        parent_permlink:"exhaust",
        parent_permlink:"hive-176853",
        author:$("#comment_author").val(),
        permlink:$("#comment_perm").val(),
        title:$("#post_title").val(),
        body:$("#id_post_body").val(),
        json_metadata:JSON.stringify(json_data),
    };
    var commentops = {
        author:$("#comment_author").val(),
        permlink:$("#comment_perm").val(),
        max_accepted_payout:"100000.000 SBD",
        percent_steem_dollars:10000,
        allow_votes:true,
        allow_curation_rewards:true,
        extensions:[[
            0,
            {"beneficiaries":[{"account":"exhaust","weight":2000}]}
        ]]
    };
    var post_operations = [
        ["comment",comment],
        ["comment_options",commentops],
    ];
    if (window.hive_keychain) {
        hive_keychain.requestBroadcast($("#comment_author").val(), post_operations, "Posting", function(response) {
            console.log('main js response - broadcast');
            console.log('creating activity via keychain');
            console.log(response);
            queue_vote()
//             if (response.success) {
    //        Queue Vote Here
//              queue_vote()
//            }
        });
    } else {
      sc_activity()
    }
});

// Send post request
$("#reply").click(function() {
    var commentops = {
        author:$("#comment_author").val(),
        permlink:$("#comment_perm").val(),
        max_accepted_payout:"100000.000 SBD",
        percent_steem_dollars:10000,
        allow_votes:true,
        allow_curation_rewards:true,
        extensions:[[
            0,
            {"beneficiaries":[{"account":"exhaust","weight":500}]}
        ]]
    };
    var json_metadata = {
        app:"exhaust-beta/0.2",
    };
    if (window.hive_keychain) {
        hive_keychain.requestPost($("#comment_author").val(), $("#comment_title").val(), $('#comment_body').val(), $("#post_pperm").val(), $("#post_author").val(), JSON.stringify(json_metadata), $("#comment_perm").val(), JSON.stringify(commentops), function(response) {
    //    steem_keychain.requestPost($("#comment_author").val(), $("#comment_title").val(), $("#comment_body").val(), $("#post_pperm").val(), $("#post_author").val(), "", $("#comment_perm").val(), "", function(response) {
            console.log('replying to activity or post via keychain');
            console.log(response);
             if (response.success) {
    //        Queue Vote Here
              queue_vote()
            }
        });
    } else {
      sc_comment()
    }
});

// AJAX for grabbing vote weight
function sc_upvote() {
    console.log("Voting via SteemConnect") // sanity check
    $.ajax({
        url : "/keychain/scVote/", // the endpoint
        type : "POST", // http method
        data : {voter: $('#upvote_username' ).val(),
		weight : $('#upvote_weight').val(),
		author: $('#upvote_author').val(),
		permlink:$('#upvote_perm').val()
	 }, // data sent with the post request

        // handle a successful response
        success : function(json) {
            console.log(json); // log the returned json to the console
            console.log("success"); // another sanity check
            location.reload();
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });
};

// AJAX for grabbing vote weight
function sc_downvote() {
    console.log("Voting via SteemConnect") // sanity check
    $.ajax({
        url : "/keychain/scVote/", // the endpoint
        type : "POST", // http method
        data : {voter: $('#downvote_username' ).val(),
                weight : $('#downvote_weight').val(),
                author: $('#downvote_author').val(),
                permlink:$('#downvote_perm').val()
         }, // data sent with the post request

        // handle a successful response
        success : function(json) {
            console.log(json); // log the returned json to the console
            console.log("success"); // another sanity check
            location.reload();
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });
};

// AJAX for grabbing vote weight
function sc_comment() {
    console.log("New Comment via SteemConnect") // sanity check
    $.ajax({
        url : "/keychain/scComment/", // the endpoint
        type : "POST", // http method
        data : {username: $('#comment_author' ).val(),
                comment_title : $('#comment_title').val(),
                comment_body: $('#comment_body').val(),
                comment_permlink:$('#comment_perm').val(),
                comment_parent_permlink:$('#post_pperm').val(),
                comment_parent_author:$('#post_author').val()
         }, // data sent with the post request

        // handle a successful response
        success : function(json) {
            console.log(json); // log the returned json to the console
            console.log("Comment sent successfully"); // another sanity check
            queue_vote();
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });
};

// AJAX for grabbing vote weight
function sc_activity() {
    console.log("New Activity via SteemConnect") // sanity check
    $.ajax({
        url : "/keychain/scPost/", // the endpoint
        type : "POST", // http method
        data : {username: $('#comment_author' ).val(),
                comment_title : $('#post_title').val(),
                comment_body: $('#id_post_body').val(),
                comment_permlink:$('#comment_perm').val(),
                comment_parent_permlink:"hive-176853",
                comment_parent_author:"",
                tags: $('#id_tags').tagsinput('items').toString(),
         }, // data sent with the post request

        // handle a successful response
        success : function(json) {
            console.log(json); // log the returned json to the console
            console.log("Comment sent successfully"); // another sanity check
            queue_vote();
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });
};

// AJAX for grabbing vote weight
function queue_vote() {
    console.log("Adding new vote to XHST queue") // sanity check
    $.ajax({
        url : "/keychain/queuevote/", // the endpoint 
        type : "POST", // http method
        data : {username: $('#comment_author').val(),
                comment_permlink:$('#comment_perm').val(),
                act_id:$('#act_id').val(),
//                weight:25
         }, // data sent with the post request

        // handle a successful response
        success : function(json) {
            console.log(json); // log the returned json to the console
            console.log("Vote added to queue!"); // another sanity check
            location.reload();
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });
};


// Send upvote request
$("#upvote").click(function() {
    console.log('trying to cast upvote');
    if (window.hive_keychain) {
        hive_keychain.requestVote($("#upvote_username").val(), $("#upvote_perm").val(), $("#upvote_author").val(), $("#upvote_weight").val(), function(response) {
            console.log('main js response - upvote');
            console.log(response);
            if (response.success) {
    //        window.location.href = data.url;
              location.reload();
            }
        });
    } else {
      sc_upvote();
    // cast vote via SteemConnect
    }
});

// Send downvote request
$("#downvote").click(function() {
    console.log('trying to cast downvote');
    if (window.hive_keychain) {
        hive_keychain.requestVote($("#downvote_username").val(), $("#downvote_perm").val(), $("#downvote_author").val(), $("#downvote_weight").val(), function(response) {
            console.log('main js response - downvote');
            console.log(response);
            if (response.success) {
     //        window.location.href = data.url;
              location.reload();
             }
        });
    } else {
        sc_downvote();
    }
});

// Send Custom JSON request
$("#resteem").click(function() {
    hive_keychain.requestCustomJson($("#resteem_username").val(), 'follow', "Posting", $("#resteem_json").val(), 'Resteem', function(response) {
        console.log('main js response - custom JSON for resteem');
        console.log(response);
    });
});

// Send Custom JSON request
$("#send_custom").click(function() {
    hive_keychain.requestCustomJson($("#custom_username").val(), $("#custom_id").val(), $("#custom_method option:selected").text(), $("#custom_json").val(), $('#custom_message').val(), function(response) {
        console.log('main js response - custom JSON');
        console.log(response);
    });
});

// Send transfer request
$("#send_tra").click(function() {
  console.log("transfer");
    hive_keychain.requestTransfer($("#transfer_username").val(), $("#transfer_to").val(), $("#transfer_val").val(), $("#transfer_memo").val(), $("#transfer_currency option:selected").text(), function(response) {
        console.log('main js response - transfer');
        console.log(response);
    }, $("#transfer_enforce").is(":checked"));
});

// Send tokens request
$("#sendTokens").click(function() {
    hive_keychain.requestSendToken($("#tokens_username").val(), $("#tokens_to").val(), $("#tokens_qt").val(), $("#tokens_memo").val(), $("#tokens_unit").val(), function(response) {
        console.log('main js response - tokens');
        console.log(response);
    });
});

// Send delegation
$("#send_delegation").click(function() {
    hive_keychain.requestDelegation($("#delegation_username").val(), $("#delegation_delegatee").val(), $("#delegation_sp").val(), $("#delegation_unit option:selected").text(), function(response) {
        console.log('main js response - delegation');
        console.log(response);
    });
});

$("#auth_login").click(function() {
    console.log("Sanity Check");
    var message = '{login:"'+$("#sign_username").val()+'"}';
    var authority = "Posting";
    hive_keychain.requestSignBuffer($("#sign_username").val(), message, authority, function(response) {
        console.log('main js response - sign');
        if (response.success){
            console.log(response);
            console.log("Successful Keychain Signature!");
            $.ajax({
                url : "hivekeychain/", // the endpoint
                type : "POST", // http method
                data : {'user':$("#sign_username").val()}, // data sent with the post request

                // handle a successful response
                success : function(json) {
                    console.log("EXHAUST Login Successful! Redirecting!"); // another sanity check
                    window.location.href = '/dash/';
//                    location.reload();
                },

                // handle a non-successful response
                error : function(xhr,errmsg,err) {
                    $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                         " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
                     console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
                }
            }).done(function (data) {
                if (data.success) {
//                    window.location.href = '/dash/';
                }
            });
        };
    });
});

$("#send_signature").click(function() {
    hive_keychain.requestSignBuffer($("#sign_username").val(), $("#sign_message").val(), $("#sign_method option:selected").text(), function(response) {
        console.log('main js response - sign');
        console.log(response);
    });
});

$("#send_addauth").click(function() {
    hive_keychain.requestAddAccountAuthority($("#addauth_username").val(), $("#addauth_authorized_username").val(), $("#addauth_role option:selected").text(), $("#addauth_weight").val(), function(response) {
        console.log('main js response - add auth');
        console.log(response);
    });
});

$("#send_removeauth").click(function() {
    hive_keychain.requestRemoveAccountAuthority($("#removeauth_username").val(), $("#removeauth_authorized_username").val(), $("#removeauth_role option:selected").text(), function(response) {
        console.log('main js response - remove auth');
        console.log(response);
    });
});

$("#send_broadcast").click(function() {
    hive_keychain.requestBroadcast($("#broadcast_username").val(), $("#broadcast_operations").val(), $("#broadcast_method option:selected").text(), function(response) {
        console.log('main js response - broadcast');
        console.log(response);
    });
});

$("#send_signed_call").click(function() {
    hive_keychain.requestSignedCall($("#signed_call_username").val(), $("#signed_call_method").val(), JSON.parse($("#signed_call_params").val()), $("#signed_call_key_type option:selected").text(), function(response) {
        console.log('main js response - signed call');
        console.log(response);
    });
});

$("#send_witness_vote").click(function() {
    hive_keychain.requestWitnessVote($("#witness_username").val(), $("#witness").val(), $("#vote_wit").is(":checked"), function(response) {
        console.log('main js response - witness vote');
        console.log(response);
    });
});

$("#send_pu").click(function() {
    hive_keychain.requestPowerUp($("#pu_username").val(), $("#pu_recipient").val(), $("#pu_steem").val(), function(response) {
        console.log('main js response - power up');
        console.log(response);
    });
});

$("#send_pd").click(function() {
    hive_keychain.requestPowerDown($("#pd_username").val(),  $("#pd_sp").val(), function(response) {
        console.log('main js response - power down');
        console.log(response);
    });
});
