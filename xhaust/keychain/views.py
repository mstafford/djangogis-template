from keychain.models import HS_Refresh

from beem.account import Account
from beem.hivesigner import HiveSigner
from beem.wallet import Wallet

from django.contrib import messages as ms
from django.contrib.auth import login, logout
from django.contrib.auth.models import User

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.template import loader
from django.views import View

from xhaust.secret import HS_SECRET, WALLET_UNLOCK

# Create your views here.
class Logout(View):
    error = "There was an unexpected error while exiting"
    success = "See you again {}"

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            ms.success(request,self.success.format(request.user))
            logout(request)
        return HttpResponseRedirect("/a/")

def login_general(request):
    context = {}
    hs = HiveSigner(client_id="exhaust.app", get_refresh_token=False, scope="login,comment,vote,offline")
    context['hs_login'] = hs.get_login_url(redirect_uri='https://beta.xhaust.me/a/hivesigner/')
    template = loader.get_template('app/login.html')
    return HttpResponse(template.render(context, request))

def LoginSignupHK(request):
    user = User.objects.get_or_create(username=request.POST['user'])[0]
    account = Account(user.username)
    if user.is_authenticated:
        login(request, user, backend="django.contrib.auth.backends.ModelBackend")
        return redirect('/dash/all/')
    else:
        return HttpResponse("No Dice w/ HiveKeychain")

def LoginSignupHS(request):
    hs = HiveSigner(client_id="exhaust.app", get_refresh_token=True, scope="login, comment, vote, offline")
    code = request.GET["code"]
    username = request.GET["username"]
    user, created = User.objects.get_or_create(username=username)
    hs.steem.wallet.unlock(WALLET_UNLOCK)
    tokens = hs.get_access_token(code)
    if user.is_authenticated:
        hs_token, created = HS_Refresh.objects.get_or_create(user=user)
        hs_token.refresh_token = tokens['refresh_token']
        hs_token.access_token = tokens['access_token']
        hs_token.save()
        login(request, user, backend="django.contrib.auth.backends.ModelBackend")
        return redirect('/dash/all/')
    else:
        return HttpResponse(f"Login Failed.")
