from django.contrib.auth.models import User
from django.db import models

# Create your models here.

class HS_Refresh(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    refresh_token = models.CharField(max_length=1024, blank=True)
    access_token = models.CharField(max_length=1024, blank=True)

