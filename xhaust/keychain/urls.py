
from django.conf.urls import url, include
from django.urls import path

from rest_framework.urlpatterns import format_suffix_patterns

from . import views

urlpatterns = [
    path('',views.login_general, name='loginsignup'),
    path('hivekeychain/', views.LoginSignupHK, name='loginsignupHK'),
    path('hivesigner/', views.LoginSignupHS, name='loginsignupHS'),
    path('logout/', views.Logout.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns,allowed=['json','html'])

